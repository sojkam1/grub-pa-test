#define _GNU_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <linux/unistd.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <sys/syscall.h>
#include <pthread.h>
#include <signal.h>
#include <stdint.h>

//#include <linux/sched/types.h>
#include <linux/sched.h>

#define gettid() syscall(__NR_gettid)

//#define SCHED_DEADLINE	6

/* XXX use the proper syscall numbers */
#ifdef __x86_64__
#define __NR_sched_setattr		314
#define __NR_sched_getattr		315
#endif

#ifdef __i386__
#define __NR_sched_setattr		351
#define __NR_sched_getattr		352
#endif

#ifdef __arm__
#define __NR_sched_setattr		380
#define __NR_sched_getattr		381
#endif

static volatile int done;

struct sched_attr {
	__u32 size;

	__u32 sched_policy;
	__u64 sched_flags;

	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;

	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;

	/* SCHED_DEADLINE (nsec) */
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

int sched_setattr(pid_t pid,
		  const struct sched_attr *attr,
		  unsigned int flags)
{
	return syscall(__NR_sched_setattr, pid, attr, flags);
}

int sched_getattr(pid_t pid,
		  struct sched_attr *attr,
		  unsigned int size,
		  unsigned int flags)
{
	return syscall(__NR_sched_getattr, pid, attr, size, flags);
}

uint64_t msec_between(struct timespec *start, struct timespec *end)
{
	return (end->tv_sec - start->tv_sec) * 1000LL +
		(end->tv_nsec - start->tv_nsec) / 1000000;
}

void *run_deadline(void *data)
{
	struct sched_attr attr;
	int x = 0;
	int ret;
	unsigned int flags = 0;

	printf("deadline thread started [%ld]\n", gettid());

	attr.size = sizeof(attr);
	attr.sched_flags = 0;
	attr.sched_flags |= SCHED_FLAG_DL_OVERRUN; /* Notify us about overruns */

	 /* Use GRUB-PA algorithm. If we use less runtime than
	  * specified in sched_runtime, the bandwidth is automatically
	  * updated and CPU frequency scaled (if used with schedutil
	  * governor). */
	attr.sched_flags |= SCHED_FLAG_RECLAIM;

	attr.sched_nice = 0;
	attr.sched_priority = 0;

	attr.sched_policy = SCHED_DEADLINE;
	attr.sched_runtime = 500 * 1000 * 1000;
	attr.sched_period = attr.sched_deadline = 1000 * 1000 * 1000;

	ret = sched_setattr(0, &attr, flags);
	if (ret < 0) {
		done = 0;
		perror("sched_setattr");
		exit(-1);
	}

	struct timespec ts_start, ts, period_start;
	uint64_t next_msec = 0;
	clock_gettime(CLOCK_MONOTONIC, &ts_start);
	period_start = ts_start;
	while (!done) {
		x++;
		clock_gettime(CLOCK_MONOTONIC, &ts);
		uint64_t msec = msec_between(&ts_start, &ts);
		if (msec >= next_msec) {
			/* Print "working" messages periodically */
			fprintf(stderr, "Working @ %zd ms\n", msec);
			while (msec >= next_msec)
				next_msec += 100;
		}
		if (msec_between(&period_start, &ts) >= attr.sched_runtime / 1000 / 1000) {
			sched_yield(); /* Wait for next period */
			clock_gettime(CLOCK_MONOTONIC, &period_start);
		}
	}

	printf("deadline thread dies [%ld]\n", gettid());
	return NULL;
}

void overrun_handler(int x)
{
	static const char *msg = "overrun\n";
	write(2, msg, strlen(msg));
}

int main (int argc, char **argv)
{
	pthread_t thread;

	printf("main thread [%ld]\n", gettid());

	signal(SIGXCPU, overrun_handler);

	pthread_create(&thread, NULL, run_deadline, NULL);

	sleep(10);

	done = 1;
	pthread_join(thread, NULL);

	printf("main dies [%ld]\n", gettid());
	return 0;
}
