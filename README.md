# Example program for testing SCHED_DEADLINE and Energy-aware scheduling under Linux


This program is based on the example from the [Linux kernel
documentation](https://www.kernel.org/doc/html/latest/scheduler/sched-deadline.html?highlight=deadline#appendix-b-minimal-main).

It creates a single SCHED_DEADLINE thread, which executes the
"workload" and prints messages about it.

Compile the program by running

    make

Then, you can run in (as root):

    ./grub-pa-test

When running on our i.MX8 test board, it might be useful to switch of
all CPUs except CPU0, to not have to deal with CPU affinity etc.:

    echo 0 | tee /sys/devices/system/cpu/cpu[^0]/online

However, note that it has some drawbacks too. For example, when the
SCHED_DEADLINE task is running, no other task can run, because the
SCHED_DEADLINE tasks have priority over normal tasks. Among other
things this means, that if you have only once CPU enabled and are
connected over SSH, you will not see the `grub-pa-test` messages until
the SCHED_DEADLINE task idles, because the SSH process cannot run. If
you want to see them as they appear, you can redirect them to the
serial console and watch the output there:

    ./grub-pa-test 2>/dev/console

By default, CPU frequency is managed by the `schedutil` governor. This
can be verified by running:

    cat /sys/devices/system/cpu/cpufreq/policy*/scaling_governor

Note that DEmOS switches the governor to `userspace`, but should
switch it back upon exit.

The schedutil governor calculates the CPU utilization by various
means, and update the CPU frequency accordingly to not run on
unnecessary high frequency.

It is useful to watch how the system changes CPU frequency. The
following commands allow watching that live:

    root@imx8qmmek:~# trace-cmd start -e power:cpu_frequency
    root@imx8qmmek:~# cat /sys/kernel/debug/tracing/trace_pipe
